# include "tdb_client.hpp"

using namespace TDb;

void show_help()
{
	// TODO
}

void set_input_mode(const bool echo)
{
	termios t;
	tcgetattr(STDIN_FILENO, &t);

	if(echo) {
		t.c_lflag |= ECHO;
	} else {
		t.c_lflag &= ~ECHO;
	}

	tcsetattr(STDIN_FILENO, TCSANOW, &t);
}

port_t get_port(string& address)
{
	size_t pos;

	if((pos = address.find(':')) != string::npos) {
		const string port(address.cbegin() + pos + 1, address.cend());
		address = string(address.cbegin(), address.cbegin() + pos);

		const auto p = stoi(port);
		if(p > 65535) throw invalid_argument("Invalid port!");

		return p;
	} else {
		return DEFAULT_PORT;
	}
}

void connect(const string& address, const port_t port)
{
	Connection connection(address, port);

	print("Username: ");

	string username;
	getline(cin, username);

	print("Password: ");

	set_input_mode(false);

	string password;
	getline(cin, password);

	set_input_mode(true);

	print("\n");

	if(!connection.login(username, password)) {
		cerr << "Login failed!" << '\n';
		exit(-1);
	}

	print("Logged in on user `");
	print(username);
	print("`!\n");

	print("\n");
	print("Welcome into TDb terminal!\n");
	print("Type a request to perform it.\n");

	thread read_thread([connection]()
		{
			while(true) {
				Data data;
				connection.fetch(data);

				// TODO
			}
		});

	while(true) {
		string request;
		getline(cin, request);

		connection.request(request);
	}
}

using namespace std;

int main(int argc, char** argv)
{
	using namespace TDb;

	for(int i = 1; i < argc; ++i) {
		const string arg(argv[i]);

		if(arg == "-h" || arg == "-help") {
			show_help();
			exit(0);
		} else if(arg == "-q" || arg == "-quiet") {
			quiet = true;
		} else {
			cout << "Unknown argument `" << arg << "`!" << '\n';
			exit(-1);
		}
	}

	try {
		print("TDb client version ");
		print(VERSION);
		print("\n");

		set_input_mode(true);

		print("Host address: ");

		string address;
		port_t port;

		getline(cin, address);
		port = get_port(address);

		connect(address, port);
	} catch(const exception& e) {
		cerr << "Error: " << e.what() << '\n';
		exit(-1);
	}

	return 0;
}
